#include <stdint.h>
#include <stdio.h>

#ifndef LOG_H
#define LOG_H

#define LOG_FILE "hash_table.log"

extern FILE *log_file; 	/* Файл для записи в лог */
extern int log_lvl;		/* Установленный уровень логирования */

typedef enum {
	NO_LOG_LEVEL = 0,
	LOG_LEVEL_WARN = 1,
	LOG_LEVEL_ERROR = 2,
	LOG_LEVEL_INFO = 4,
	LOG_LEVEL_DEBUG = 8,
} log_level;

/* Печать стэка вызовов */
void fprintf_stack();

/* Инициализация логера */
void loger_init(char *log_name, char *lvl);

/* Закрытие файла для логирование */
void loger_free();

/* Запись в файл для логирования */
void loging(const char *file, long line, uint8_t level,	const char *format, ...);

#define log_error(...) if (log_lvl >= LOG_LEVEL_ERROR) {loging(__FILE__, __LINE__, LOG_LEVEL_ERROR, __VA_ARGS__); fprintf_stack();}
#define log_errno() if (log_lvl >= LOG_LEVEL_ERROR) {char *errorbuf = strerror(errno); loging(__FILE__, __LINE__, LOG_LEVEL_ERROR, errorbuf); fprintf_stack();}
#define log_warn(...) if (log_lvl >= LOG_LEVEL_WARN) loging(__FILE__, __LINE__, LOG_LEVEL_WARN, __VA_ARGS__)
#define log_info(...) if (log_lvl >= LOG_LEVEL_INFO) loging(__FILE__, __LINE__, LOG_LEVEL_INFO, __VA_ARGS__)
#define log_debug(...) if (log_lvl >= LOG_LEVEL_DEBUG) loging(__FILE__, __LINE__, LOG_LEVEL_DEBUG, __VA_ARGS__)

#endif /* LOG_H */
