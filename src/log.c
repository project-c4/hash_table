#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <execinfo.h>
#include <errno.h>
#include <stdarg.h>
#include "log.h"

#define MAX_SIZE 16

FILE *log_file;                 /* Файл для записи в лог */
int log_lvl = LOG_LEVEL_ERROR;  /* Установленный уровень логирования */

/* Инициализация логера */
void loger_init(char *log_name, char *lvl)
{
    if (strcmp(lvl, "WARN") == 0)
    {
        log_lvl = LOG_LEVEL_WARN;
    }
    if (strcmp(lvl, "INFO") == 0)
    {
        log_lvl = LOG_LEVEL_INFO;
    }
    if (strcmp(lvl, "DEBUG") == 0)
    {
        log_lvl = LOG_LEVEL_DEBUG;
    }
    if (strcmp(lvl, "NO_LOG") == 0)
    {
        log_lvl = NO_LOG_LEVEL;
    }

    if (log_name != NULL)
    {
        if ((log_file = fopen(log_name, "w")) == NULL)
        {
            char *errorbuf = strerror(errno);
            fprintf(stderr, "WARN : %s:%d : log file not open : %s\n", __FILE__, __LINE__, errorbuf);
            errno = 0;
            return;
        }
    }
    else
    {    
        if ((log_file = fopen(LOG_FILE, "w")) == NULL)
        {
            char *errorbuf = strerror(errno);
            fprintf(stderr, "WARN : %s:%d : log file not open : %s\n", __FILE__, __LINE__, errorbuf);
            errno = 0;
            return;
        }
    }
}

/* Запись в файл для логирования */
void loging(const char *file,
	long line, uint8_t lvl,
	const char *format, ...)
{
    if (log_file == NULL)
    {
        return;
    }

    va_list va;
    char *l;
    char buffer[256];
    switch(lvl)
    {
        case 1: l = "[ ERROR ]"; break;
        case 2: l = "[ WARN  ]"; break;
        case 4: l = "[ INFO  ]"; break;
        case 8: l = "[ DEBUG ]"; break;
        default: break;
    }
    va_start(va, format);
    vsnprintf(buffer, 256, format, va);
    va_end(va);
    fprintf(log_file, "%s : %s[%ld]: %s\n", 
    l, file, line, buffer);
}

/* Закрытие файла для логирование */
void loger_free()
{
    if (log_file != NULL)
    {
        fclose(log_file);
    }
}

/* Печать стэка вызовов */
void fprintf_stack()
{
    if (log_file == NULL)
    {
        return;
    }

    void *trace[MAX_SIZE];
    char **messages = NULL;
    int size = 0;

    size = backtrace(trace, MAX_SIZE);
    messages = backtrace_symbols(trace, size);
    for (int i = 1; i < size; i++)
    {
        fprintf(log_file, "    %s\n", messages[i]);
    }
}

