#include <sys/stat.h>
#include <string.h>
#include <errno.h>

#include "log.h"
#include "hash_table.h"

int main(int argc, char *argv[])
{
    hash_table *table;			            /* хэш таблица */
    FILE *file;                             /* указатель на файл с текстом */
    struct stat info_file;                  /* Информация о файле */
    char *str;                              /* Строка для считывания файла */
    char *pword;                            /* Указатель на начальную букву слова в строке */
    char *pword_end;                        /* Указатель на конечную букву слова в строке */
    errno = 0;

    if (argc == 3)
    {
        loger_init(NULL, argv[2]);
    }
    else 
    {
        loger_init(NULL, "ERROR");
    }

    for (int i = 0; i < argc; i++)
    {
        if (strcmp(argv[i], "-h") == 0)
        {
            printf("\n./programm [file_name.format]\n\n");
            return 0;
        }
    }

    if (argc > 3 || argc == 1)
    {
        log_error("inappropriate number of parameters");
        return -1;
    }
    
    log_info("Start programm");
    table = init_hash_table(); 
    log_info("Reading file");
    if ((file = fopen(argv[1], "rb")) == NULL)
    {
        log_errno();
        return errno;
    }
    stat(argv[1], &info_file);

    str = (char *) calloc(info_file.st_size + 1, sizeof(char));
    if (errno != 0)
    {
        log_errno();
        return errno;
    }
    fread(str, 1, info_file.st_size, file);

    pword = str;
    pword_end = strtok(str, " ,.!?()\n\r\0");
    log_info("Populating the hash table");
    while (pword_end  != NULL)
    {
        uint8_t size = pword_end - pword;
        if(size != 0)
        {
            char *word = (char *) calloc(size + 1, sizeof(char));
            memcpy(word, pword, size);
            insert(table, word);
            if (errno != 0)
            {
                log_errno();
                return errno;
            }
            free(word);
        }
        pword = pword_end;
        pword_end = strtok(NULL, " ,.!?()\n\r\0");
    }

    log_info("Printing hash table");
    print_hash_table(table);

    free_hash_table(table);
    log_info("Cleaning data");
    free(str);
    fclose(file);
    loger_free();
    return 0;
}
