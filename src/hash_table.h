#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#ifndef HASH_TABLE_H
#define HASH_TABLE_H

#define REHASH_SIZE 0.75    /* коэффициент, при котором произойдет увеличение таблицы */
#define REHASH 0            /* коэффициент, при котором произойдёт перехэширование таблицы */
#define DEFAULT_SIZE 256    /* Начальный размер хэш таблицы */
#define KEY 256             /* Ключ для хэширования */

typedef struct node     /* Словарь для хэш таблицы */
{
    char *word;         /* Слово*/
    uint32_t hash;       /* Хэш этого слова */
    uint32_t count;     /* Количество слов */
    uint8_t state;      /* Состояние блока (удалено/не удалено) */
}node;

typedef struct hash_table   /* Структура хэш таблицы */
{
    node *arr;                                  /* Указатель на элементы хэш таблицы */
    uint32_t real_size;                         /* Размер без учёта удалённых */
    uint32_t size;                              /* Размер с учётом удалённых */
    uint32_t all_size;                          /* Размер всей выделенной памяти под словари */
}hash_table;

/* Инициализация хэш таблицы */
hash_table *init_hash_table();

/* функция хэширования */
uint64_t hash_func(char *word, uint32_t table_size, int32_t key);

/* функция вставки словаря из хэш таблицы */
void insert(hash_table *table, char *word);

/* функция удаления словаря из хэш таблицы */
void remove_node(hash_table *table, char *word);

/* Поиск словаря в хэш таблице */
node *find_node(hash_table *table, char *word);

/* Очищение удалённых словарей */
void rehash(hash_table *table);

/* Подсчёт частоты встречаемости слов с тексте */
void freq_word(hash_table *table, char *str);

/* Печать хэш таблицы */
void print_hash_table(hash_table *table);

/* Очистка хэш таблицы */
void free_hash_table(hash_table *table);

#endif /* HASH_TABLE_H */
