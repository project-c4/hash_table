#include <string.h>
#include <errno.h>

#include "hash_table.h"
#include "log.h"

/* Инициализация хэш таблицы */
hash_table *init_hash_table()
{
    log_debug("%s", "Initialization hash-table", "Initialization hash-table");
    hash_table *table = (hash_table *) malloc(sizeof(hash_table));
    table->all_size = DEFAULT_SIZE;
    table->size = 0;
    table->real_size = 0;
    if ((table->arr = (node *) calloc(table->all_size, sizeof(node))) == NULL)
    {
    	
	    log_errno();
	    return NULL;
    }
    return table;
}

/* функция хэширования */
uint64_t hash_func(char *word, uint32_t table_size, int32_t key)
{
    log_debug("hashing \"%s\"", word);
    uint64_t result = 0;
    for (uint32_t i = 0; i < strlen(word); i++)
    {
        result = (result * key + *(word + i)) % table_size;
    }
    result = ((result << 1)  + 1) % table_size;
    return result;
}

/* функция вставки словаря из хэш таблицы */
void insert(hash_table *table, char *word)
{
    log_debug("insert \"%s\"", word);
    uint64_t hash;
    hash = hash_func(word, table->all_size, KEY);
    for (uint32_t i = 0; i < table->all_size; i++)
    {
        if ((table->arr + hash)->word == NULL)
        {
            table->real_size += 1;
            table->size += 1;
            if (table->size > table->all_size * REHASH_SIZE)
            {
                rehash(table);
                if (errno != 0)
                {
                    errno = 0;
                    log_warn("data loss : %s", word);
                    return;
                }
                hash = hash_func(word, table->all_size, KEY);
                i = 0;
                continue;
            }
            if (((table->arr + hash)->word = (char *) calloc(strlen(word) + 1, sizeof(char))) == NULL)
	        {
                log_warn("data loss : %s", word);
                errno = 0;
                return;
	        }
            strcpy((table->arr + hash)->word, word);
            (table->arr + hash)->hash = hash;
            (table->arr + hash)->state = 1;
            (table->arr + hash)->count = 1;
            break;
        }
        else if (strcmp((table->arr + hash)->word, word) == 0)
        {
            if ((table->arr + hash)->state == 1)
            {
                (table->arr + hash)->count += 1;
            }
            break;
        }
        
        hash += 1;
        if (hash == table->all_size)
        {
            hash = 0;
        }
    }
}

/* функция удаления словаря из хэш таблицы */
void remove_node(hash_table *table, char *word)
{
    log_debug("removing \"%s\"", word);
    uint64_t hash;
    hash = hash_func(word, table->all_size, KEY);
    for (uint32_t i = 0; i < table->all_size; i++)
    {
        if (strcmp((table->arr + hash)->word, word) == 0)
        {
            (table->arr + hash)->state = 0;
            table->real_size -= 1;
            if ((((int32_t) table->real_size << 1) - (int32_t) table->size) < REHASH)
            {
                rehash(table);
            }
            break;
        }
        else if ((table->arr + hash)->word == NULL)
        {
            break;
        }
        hash += 1;
        if (hash == table->all_size)
        {
            hash = 0;
        }
    }
}

// /* Поиск словаря в хэш таблице */
// node *find_node(hash_table *table, char *word)
// {
//     uint64_t hash;
//     hash = hash_func(word, table->all_size, KEY);
//     for (uint32_t i = 0; i < table->all_size; i++)
//     {
//         if ((table->arr + hash)->word == NULL)
//         {
//             return NULL;
//         }
//         else if (strcmp((table->arr + hash)->word, word) == 0)
//         {
//             return table->arr + hash;
//         }
//         hash += 1;
//         if (hash == table->all_size)
//         {
//             hash = 0;
//         }
//     }
//     return NULL;
// }

/* Увеличение размера хэш таблицы */
void rehash(hash_table *table)
{
    log_debug("%s", "rehash table");
    uint64_t hash;
    node *new_arr;
    uint32_t old_size = table->all_size;
    if (table->size > REHASH_SIZE * table->all_size)
    {
        table->all_size += DEFAULT_SIZE;
    }
    if ((new_arr = (node *) calloc(table->all_size, sizeof(node))) == NULL)
    {
    	log_warn("%s", "the table could not be rehashed");
	    return;
    }
    for (node *i = table->arr; i - table->arr < old_size; i++)
    {
        if (i->word != NULL)
        {
            if (i->state != 0)
            {
                hash = hash_func(i->word, table->all_size, KEY);
            }
            else
            {
                free(i->word);
                continue;
            }
            
        }
        else
        {
            continue;
        }
        for (uint32_t j = 0; j < table->all_size; j++)
        {
            if ((new_arr + hash)->word == NULL)
            {
                (new_arr + hash)->word = (char *) calloc(strlen(i->word) + 1, sizeof(char));
                strcpy((new_arr + hash)->word, i->word);
                (new_arr + hash)->hash = hash;
                (new_arr + hash)->state = 1;
                (new_arr + hash)->count = i->count;
                free(i->word);
                break;
            }
            else if (strcmp((new_arr + hash)->word, i->word) == 0)
            {
                break;
            }
         
            hash += 1;
            if (hash == table->all_size)
            {
                hash = 0;
            }
        }
    }
    free(table->arr);
    table->arr = new_arr;
}

// /* Подсчёт частоты встречаемости слов с тексте */
// void freq_word(hash_table *table, char *str)
// {
//     char *pword = str;
//     for (uint32_t i = 0; i < strlen(str); i++)
//     {
//         if (str[i] == ' ' || str[i] == ',' || str[i] == '.' || str[i] == '!' || str[i] == '?')
//         {
//             if(&str[i] - pword > 1)
//             {
//                 node *p;
//                 char *word = (char *) calloc(i + 1, sizeof(char));
//                 memcpy(word, pword, &str[i] - pword);
//                 if ((p = find_node(table, word)) != NULL)
//                 {
//                     p->count += 1;
//                 }
//                 free(word);
//             }
//             pword = &str[i + 1];
//         }
//     }
// }

/* Печать хэш таблицы */
void print_hash_table(hash_table *table)
{
    for (node *i = table->arr; i - table->arr < table->all_size; i++)
    {
        if (i->word != NULL && i->state != 0)
        {
            printf("%s - %d\n", i->word, i->count);
        }
    }
}

/* Очистка хэш таблицы */
void free_hash_table(hash_table *table)
{
    for(node *i = table->arr; i - table->arr < table->all_size; i++)
    {
        if (i->word != NULL)
        {
            free(i->word);
        }
    }
    free(table->arr);
    free(table);
}
